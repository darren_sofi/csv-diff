package com.sofi.csvdiff;

import com.sofi.csvdiff.cmdline.ParsedCommandLine;
import com.sofi.csvdiff.csv.Row;
import com.sofi.csvdiff.csv.Sheet;
import com.sofi.csvdiff.csv.SheetCollector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author darrenkennedy
 *
 * Main class
 * Reads and parses command line
 * Reads input files
 * Writes output file
 **/
public class Diff {
    private String idColumn;
    private boolean zeroMatchesNull;

    private Sheet sheetA;
    private Sheet sheetB;
    private List<String> headers;
    private List<String> headersMissingA;
    private List<String> headersMissingB;
    private boolean hasDiff;

    /**
     *  Main entry point
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        ParsedCommandLine cmd = new ParsedCommandLine();
        if (cmd.parseCommandLine(args)) {
            new Diff().makeDiff(cmd);
        }
    }

    /**
     * Default constructor
     */
    public Diff() {
        hasDiff = false;
    }

    /**
     * Reads the input files,
     * finds the differences and
     * writes the output file.
     * @param cmd The command line arguments as class fields
     * @throws IOException
     */
    public void makeDiff(ParsedCommandLine cmd) throws IOException {
        idColumn = cmd.getIdColumn();
        sheetA = new Sheet(idColumn);
        sheetB = new Sheet(idColumn);

        try {
            sheetA.parse(cmd.getFilenameA());
        } catch (Exception e) {
            System.err.println("Failed to parse csv file: " + cmd.getFilenameA());
            throw e;
        }
        try {
            sheetB.parse(cmd.getFilenameB());
        } catch (Exception e) {
            System.err.println("Failed to parse csv file: " + cmd.getFilenameB());
            throw e;
        }

        if (!getHeaders()) {
            return;
        }
        printMissingColumns(cmd.getFilenameA(), headersMissingA);
        printMissingColumns(cmd.getFilenameB(), headersMissingB);

        // Find all records from A that have records (by key) in B
        // Map the CSVRecord to get the diff string if cells in the record don't match,
        // or an empty string if we are not displaying matching values,
        // or the matching value if we are displaying them.
        // Add rows to a new sheet
        zeroMatchesNull = cmd.isZeroMatchesNull();
        Collector<Row, Sheet, Sheet> c = new SheetCollector(idColumn);
        Sheet sheetDiff = sheetA
                        .getRows()
                        .stream()
                        .filter(r -> sheetB.contains(r.getId()))
                        .map(r -> getDiff(r, sheetB.getRow(r.getId()), cmd.isDisplayMatchingData()))
                        .filter(r -> !r.isEmpty())
                        .collect(c);
        if (sheetDiff.getRows().size() == 0) {
            System.out.println("No matching records found. Quitting");
            return;
        }

        List<Row> recordsMissingA = getMissingRecords(sheetA, sheetB);
        List<Row> recordsMissingB = getMissingRecords(sheetB, sheetA);

        printMissingRecords(cmd.getFilenameA(), recordsMissingA);
        printMissingRecords(cmd.getFilenameB(), recordsMissingB);

        getDiffHeaders();

        if (!hasDiff) {
            System.out.println("Records common to both files are identical.");
            if (recordsMissingA.isEmpty() && recordsMissingB.isEmpty()) {
                System.out.println("Both files have the same records.");
                if (headersMissingA.isEmpty() && headersMissingB.isEmpty()) {
                    System.out.println("Both files have the same headers.");
                    System.out.println("Files are identical. Quitting.");
                    return;
                }
            }
        }

        if (cmd.isDisplayMissingRows()) {
            addMissingRecords(sheetDiff, recordsMissingA, recordsMissingB);
        }

        sheetDiff.write(cmd.getFilenameOut());
        System.out.println("Wrote diffs to :" + cmd.getFilenameOut());
    }

    private boolean getHeaders() {
        headers = sheetA
                        .getHeaders()
                        .stream()
                        .filter(h -> sheetB.getHeaders().contains(h))
                        .sorted((h1, h2) -> h1.compareTo(h2))
                        .collect(Collectors.toList());

        headersMissingB = sheetA
                        .getHeaders()
                        .stream()
                        .filter(h -> !headers.contains(h))
                        .collect(Collectors.toList());

        headersMissingA = sheetB
                        .getHeaders()
                        .stream()
                        .filter(h -> !headers.contains(h))
                        .collect(Collectors.toList());

        if (headers.isEmpty()) {
            System.out.println("No matching column headers found. Quitting.");
            return false;
        }

        return true;
    }

    private void getDiffHeaders() {
        ArrayList<String> diffHeaders = new ArrayList<String>(headers);

        headersMissingA
            .stream()
            .filter(h -> !headers.contains(h))
            .forEach(h -> diffHeaders.add("<< " + h));

        headersMissingB
            .stream()
            .filter(h -> !headers.contains(h))
            .forEach(h -> diffHeaders.add(">> " + h));
    }

    private void printMissingColumns(String filename, List<String> headersMissing) {
        System.out.println("Columns missing from " + filename + ":");
        headersMissing
            .stream()
            .forEach(System.out::println);
        System.out.println("<end>\n");
    }

    private List<Row> getMissingRecords(Sheet missingFrom, Sheet checkAgainst) {
        return checkAgainst
            .getRows()
            .stream()
            .filter(r -> !missingFrom.contains(r.getId()))
            .sorted((r1, r2) -> r1.getId().compareTo(r2.getId()))
            .map(r -> getDiff(r, r, true))
            .collect(Collectors.toList());
    }

    private void printMissingRecords(String filename, List<Row> recordsMissing) {
        System.out.println("Records not found in " + filename + ":");
        recordsMissing
            .stream()
            .sorted()
            .map(r -> r.getId())
            .forEach(System.out::println);
        System.out.println("<end>\n");
    }

    private void addMissingRecords(Sheet sheetDiff, List<Row> recordsMissingA, List<Row> recordsMissingB) {
        if (!recordsMissingA.isEmpty()) {
            sheetDiff.addRow(new Row(new String[]{"<<<<<<"}, new String[]{idColumn}, idColumn));
            recordsMissingA
                     .stream()
                     .forEach(r -> sheetDiff.addRow(new Row(r, "<<" + r.getId())));
        }

        if (!recordsMissingB.isEmpty()) {
            sheetDiff.addRow(new Row(new String[]{">>>>>>"}, new String[]{idColumn}, idColumn));
            recordsMissingB
                     .stream()
                     .forEach(r -> sheetDiff.addRow(new Row(r, ">>" + r.getId())));
        }
    }

    private Row getDiff(Row r1, Row r2, boolean showIdentical) {
        return new Row(headers
                .stream()
                .map(h -> {
                    if (h.equals(idColumn))
                        return r1.getId();

                    String h1 = r1.getValue(h);
                    String h2 = r2.getValue(h);
                    if (h1.equals(h2))
                        return showIdentical ? h1 : "";
                    else if (zeroMatchesNull && "0".equals(h1 + h2))
                        return showIdentical ? "0" : "";

                    String diff = "";
                    if (!h1.isEmpty())
                        diff = r1.getValue(h) + "<<";
                    if (!h2.isEmpty())
                        diff = diff.concat(">>" + r2.getValue(h));
                    if (!diff.isEmpty())
                        hasDiff = true;
                    return diff;
                })
                .collect(Collectors.toList()),
            headers,
            idColumn);
    }

}
