package com.sofi.csvdiff.csv;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.RFC4180Parser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents a CSV file with an id column
 * or the differences between two CSV files.
 *
 * @author darrenkennedy
 */
public class Sheet {
    private CSVReader reader;
    private Map<String, Row> rows;
    private Set<String> cols;
    private String idColumn;

    public Sheet(String idColumn) {
        this.idColumn = idColumn;
        this.rows = new LinkedHashMap<>();
        this.cols = new LinkedHashSet<>();
    }

    public void parse(String filename) throws IOException {
        open(filename);
        try {
            cols.addAll(Arrays.asList(reader.readNext()));

            if (!cols.contains(idColumn)) {
                throw new IllegalArgumentException("Id column '" + idColumn + "' not found in " + filename);
            }

            reader
                .readAll()
                .stream()
                .forEach(r -> {
                    Row r2 = new Row(r, cols.toArray(new String[0]), idColumn);
                    rows.put(r2.getId(), r2);
                });
        } finally {
            reader.close();
        }
    }

    public void write(String filename) throws IOException {
        try (CSVWriter writer = new CSVWriter(new FileWriter(filename), ',')) {
            writer.writeNext(cols.toArray(new String[0]));
            rows
                .values()
                .stream()
                .forEach(r -> writer.writeNext(r.toArray(new String[0])));
        }
    }

    private void open(String filename) throws IOException {
        reader = new CSVReader(new FileReader(filename), 0, new RFC4180Parser());
    }

    public Collection<String> getHeaders() {
        return this.cols;
    }

    public Collection<Row> getRows() {
        return this.rows.values();
    }

    public Row getRow(String id) {
        return this.rows.get(id);
    }

    public boolean contains(String id) {
        return this.rows.containsKey(id);
    }

    public String getIdColumn() {
        return this.idColumn;
    }

    public void addRow(Row row) {
        cols.addAll(row.getHeaders());
        rows.put(row.getId(), row);
    }
}
