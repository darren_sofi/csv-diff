package com.sofi.csvdiff.csv;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

public class Row implements Comparable<Row> {
    private String id;
    private String idColumn;
    // Map<ColName, value>
    private Map<String, String> colVals;

    public Row(String[] values, String[] headers, String idColumn) {
        this(Arrays.asList(values), headers, idColumn);
    }

    public Row(List<String> values, List<String> headers, String idColumn) {
        this(values, headers.toArray(new String[0]), idColumn);
    }

    public Row(List<String> values, String[] headers, String idColumn) {
        this.colVals = new HashMap<>();
        for (int i = 0; i < values.size(); i++) {
            this.colVals.put(headers[i], values.get(i));
        }

        this.id = colVals.get(idColumn);
        this.idColumn = idColumn;
    }

    public Row(Row row, String newId) {
        this.colVals = new HashMap<>();
        this.colVals.putAll(row.colVals);
        this.id = newId;
        this.idColumn = row.idColumn;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.colVals.put(idColumn, id);
        this.id = id;
    }

    public String getValue(String colName) {
        return this.colVals.get(colName);
    }

    public Set<String> getHeaders() {
        return this.colVals.keySet();
    }

    public String[] toArray(String[] values) {
        return colVals.values().toArray(values);
    }

    public boolean isEmpty() {
        for (Entry<String, String> e: colVals.entrySet()) {
            if (!e.getValue().isEmpty() && !e.getKey().equals(idColumn)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int compareTo(Row o) {
        if (this.equals(o))
            return 0;

        return this.id.compareTo(((Row) o).getId());
    }

    /**
     * Equality is based on the id value only
     */
    @Override
    public boolean equals(Object obj)
    {
       if (obj == null) {
          return false;
       }
       if (getClass() != obj.getClass()) {
          return false;
       }
       final Row other = (Row)obj;
       return Objects.equals(this.id, other.id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}
